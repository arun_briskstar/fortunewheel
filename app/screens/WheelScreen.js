/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet,
    View,
    Text as RNText,
    Dimensions,
    Animated,
    Alert
    } from 'react-native';
    
import NetInfo from "@react-native-community/netinfo";

import Svg,{Path, G, Text, TSpan } from "react-native-svg";
import GestureHandler,{ PanGestureHandler,State } from 'react-native-gesture-handler';
import * as d3Shape from 'd3-shape';
import color from 'randomcolor';
import { snap } from '@popmotion/popcorn';
import strings from '../res/strings';

import DeviceInfo from 'react-native-device-info';
import * as AppConstant from "../Utils/AppConstant";

const R = {
  strings
}


//const { PanGestureHandler, State } = GestureHandler;
const { width } = Dimensions.get('screen');

const numberOfSegments = 12;
const wheelSize = width * 0.95;
const fontSize = 26;
const oneTurn = 360;
const angleBySegment = oneTurn / numberOfSegments;
const angleOffset = angleBySegment / 2;
const knobFill = color({ hue: 'purple' });

const makeWheel = () => {
  const data = Array.from({ length: numberOfSegments }).fill(1);
  const arcs = d3Shape.pie()(data);
  const colors = color({
    luminosity: 'dark',
    count: numberOfSegments
  });

  return arcs.map((arc, index) => {
    const instance = d3Shape
      .arc()
      .padAngle(0.01)
      .outerRadius(width / 2)
      .innerRadius(20);

    return {
      path: instance(arc),
      color: colors[index],
      value: Math.round(Math.random() * 10 + 1) * 200, //[200, 2200]
      centroid: instance.centroid(arc)
    };
  });
};

const netInfoNew = useNetInfo();
export default class WheelScreen extends Component<Props> {
    _wheelPaths = makeWheel();
    _angle = new Animated.Value(0);
    angle = 0;
  
    state = {
      enabled: true,
      finished: false,
      winner: null,
      alertShowing: true
    };

      componentWillMount() {

      }

      componentWillUnmount() {
        NetInfo.removeEventListener('connectionChange', this.handleFirstConnectivityChange);

        /* BackHandler.removeEventListener(
          "hardwareBackPress",
          this.handleBackButtonClick
        ); */
      }
  
    componentDidMount() {

    NetInfo.addEventListener('connectionChange', this.handleFirstConnectivityChange);

        NetInfo.isConnected.fetch().done((isConnected) => {
          if (isConnected == true) {
            this.setState({
              isOnline: isConnected
            })
          }
          else {
            this.setState({
              isOnline: isConnected
            })
          }
        });

      this._angle.addListener(event => {
        if (this.state.enabled) {
          this.setState({
            enabled: false,
            finished: false
          });
        }
  
        this.angle = event.value;
      });

      this.doAnimate();

    }

    handleFirstConnectivityChange = isConnected => {
        // NetInfo.isConnected.removeEventListener(
        //   "connectionChange",
        //   this.handleFirstConnectivityChange
        // );
        console.log("inside handleFirstConnectivityChange" );
        NetInfo.isConnected.fetch().then(isConnected => {
          if (isConnected) {
            //this.dogenerateToken(this.state.nameValue, this.state.pwdValue);
            this.setState({
              isOnline: true
            })
          } else {
            this.setState({
              isOnline: false
            })
          }
        });


        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this.handleFirstConnectivityChange
        );
        // this.setState({
        //   isOnline: isConnected
        // })
      };



      doCallPriceAPI(name: String, email: String,phone: String) {

            this.setState({ isLoading: true });
            const DeviceId = DeviceInfo.getDeviceId();
            console.log(JSON.stringify({
              Name: name,
              Email: email,
              MobilNo: phone,
              DeviceID: DeviceId
            }))
            fetch(AppConstant.LoginApi, {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify({
                Name: name,
                Email: email,
                MobilNo: phone,
                DeviceID: DeviceId
              })
            })
              .then(response => response.json())
              .then(responseJson => {
                console.log(responseJson)
                this.setState({
                        isLoading: false,
                        enabled: false,
                        finished: false
                    });
                var data = responseJson;

                console.log("data detailed" + data);
                              
              })
              .catch(error => {
                this.setState({ isLoading: false });
                console.error(error);
              });
            //},2500);
          }
  
    _getWinnerIndex = () => {
      const deg = Math.abs(Math.round(this.angle % oneTurn));
      return Math.floor(deg / angleBySegment);
    };
  
    _onPan = ({ nativeEvent }) => {
      if (nativeEvent.state === State.END) {
        const { velocityY } = nativeEvent;
  
        Animated.decay(this._angle, {
          velocity: velocityY / 1000,
          deceleration: 0.999,
          useNativeDriver: true
        }).start(() => {
          this._angle.setValue(this.angle % oneTurn);
          const snapTo = snap(oneTurn / numberOfSegments);
          Animated.timing(this._angle, {
            toValue: snapTo(this.angle),
            duration: 300,
            useNativeDriver: true
          }).start(() => {
            const winnerIndex = this._getWinnerIndex();
            this.setState({
              enabled: true,
              finished: true,
              //winner: this._wheelPaths[winnerIndex].value
              setDialog: true
            });
            if(this.state.setDialog){
              Alert.alert(
                'Alert Title',
                'My Alert Msg',
                [
                  {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
            }
          });
          // do something here;
        });
      }
    };

    render() {
      return (
        <PanGestureHandler
          onHandlerStateChange={this._onPan}
          enabled={this.state.enabled}
        >
          <View style={styles.container}>
            {this._renderSvgWheel()}
            {this.state.finished && this.state.enabled && this._renderWinner()}
          </View>
        </PanGestureHandler>
        
      );
    }
  
    _renderKnob = () => {
      const knobSize = 30;
      // [0, numberOfSegments]
      const YOLO = Animated.modulo(
        Animated.divide(
          Animated.modulo(Animated.subtract(this._angle, angleOffset), oneTurn),
          new Animated.Value(angleBySegment)
        ),
        1
      );
  
      return (
        <Animated.View
          style={{
            width: knobSize,
            height: knobSize * 2,
            justifyContent: 'flex-end',
            zIndex: 1,
            transform: [
              {
                rotate: YOLO.interpolate({
                  inputRange: [-1, -0.5, -0.0001, 0.0001, 0.5, 1],
                  outputRange: ['0deg', '0deg', '35deg', '-35deg', '0deg', '0deg']
                })
              }
            ]
          }}
        >
          <Svg
            width={knobSize}
            height={(knobSize * 100) / 57}
            viewBox={`0 0 57 100`}
            style={{ transform: [{ translateY: 8 }] }}
          >
            <Path
              d="M28.034,0C12.552,0,0,12.552,0,28.034S28.034,100,28.034,100s28.034-56.483,28.034-71.966S43.517,0,28.034,0z   M28.034,40.477c-6.871,0-12.442-5.572-12.442-12.442c0-6.872,5.571-12.442,12.442-12.442c6.872,0,12.442,5.57,12.442,12.442  C40.477,34.905,34.906,40.477,28.034,40.477z"
              fill={knobFill}
            />
          </Svg>
        </Animated.View>
      );
    };

    doAnimate(){
    this.setState({
            enabled: false,
            finished: false
    });
      console.log("inside doAnim");
      
        const name = this.props.navigation.state.params.name;
        const email = this.props.navigation.state.params.email;
        const phone = this.props.navigation.state.params.phone;
  
        this.handleFirstConnectivityChange();
        if (this.state.isOnline) {
            console.log("online true");
            this.doCallPriceAPI(name,email,phone);
        } else {
          console.log("not online");
          if (this.state.alertShowing) {
            this.state.alertShowing = false
            Alert.alert(
              R.strings.common.app_title,
              R.strings.common.msg_internet_connection,
              [
                {
                  text: R.strings.login.lbl_ok,
                  onPress: () => { this.state.alertShowing = true }
                }
              ],
              { cancelable: false }
            );
          }
        }
    }
  
    _renderWinner = () => {
      return (
        <RNText style={styles.winnerText}>Winner is: {this.state.winner}</RNText>
      );
    };
  
    _renderSvgWheel = () => {
      return (
        <View style={styles.container}>
          {this._renderKnob()}
          <Animated.View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              transform: [
                {
                  rotate: this._angle.interpolate({
                    inputRange: [-oneTurn, 0, oneTurn],
                    outputRange: [`-${oneTurn}deg`, `0deg`, `${oneTurn}deg`]
                  })
                }
              ]
            }}
          >
            <Svg
              width={wheelSize}
              height={wheelSize}
              viewBox={`0 0 ${width} ${width}`}
              style={{ transform: [{ rotate: `-${angleOffset}deg` }] }}
            >
              <G y={width / 2} x={width / 2}>
                {this._wheelPaths.map((arc, i) => {
                  const [x, y] = arc.centroid;
                  const number = arc.value.toString();
  
                  return (
                    <G key={`arc-${i}`}>
                      <Path d={arc.path} fill={arc.color} />
                      <G
                        rotation={(i * oneTurn) / numberOfSegments + angleOffset}
                        origin={`${x}, ${y}`}
                      >
                        <Text
                          x={x}
                          y={y - 70}
                          fill="white"
                          textAnchor="middle"
                          fontSize={fontSize}
                        >
                          {Array.from({ length: number.length }).map((_, j) => {
                            return (
                              <TSpan
                                x={x}
                                dy={fontSize}
                                key={`arc-${i}-slice-${j}`}
                              >
                                {number.charAt(j)}
                              </TSpan>
                            );
                          })}
                        </Text>
                      </G>
                    </G>
                  );
                })}
              </G>
            </Svg>
          </Animated.View>
        </View>
      );
    };
  }

  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    winnerText: {
      fontSize: 32,
      fontFamily: 'Menlo',
      position: 'absolute',
      bottom: 10
    }
  });
