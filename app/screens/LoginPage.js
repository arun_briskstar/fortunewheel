import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput} from 'react-native';
import strings from '../res/strings';

import { RaisedButton } from "react-native-material-buttons";
import DeviceInfo from 'react-native-device-info';
import NetInfo from "@react-native-community/netinfo";

import * as AppConstant from "../Utils/AppConstant";

const R = {
    strings
  }

export default class LoginPage extends Component<Props> {

    constructor(props) {
      super(props);
      this.state = {
        nameError: "",
        pwdError: "",
        pwdValue: "",
        isLoading: false,
        isOnline: true,
        alertShowing: true,
        nameValue:"",
        emailValue:"",
        phoneValue:""
        
      };
    }
  componentWillMount() {

  }
  componentWillUnmount() {
    NetInfo.removeEventListener('connectionChange', this.handleFirstConnectivityChange);

    /* BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    ); */
  }

  /* handleBackButtonClick() {
    Alert.alert(
      R.strings.login.lbl_exit,
      R.strings.login.msg_exit_app,
      [
        {
          text: R.strings.login.lbl_cancel,
          onPress: () => {
            return true;
          }
        },
        {
          text: R.strings.login.lbl_yes,
          onPress: () => {
            BackHandler.exitApp();
          }
        }
      ],
      { cancelable: false }
    );
    return true;
  } */



  componentDidMount() {

    NetInfo.addEventListener('connectionChange', this.handleFirstConnectivityChange);

    NetInfo.isConnected.fetch().done((isConnected) => {
      if (isConnected == true) {
        this.setState({
          isOnline: isConnected
        })
      }
      else {
        this.setState({
          isOnline: isConnected
        })
      }
    });

  }

  handleFirstConnectivityChange = isConnected => {
    // NetInfo.isConnected.removeEventListener(
    //   "connectionChange",
    //   this.handleFirstConnectivityChange
    // );
    console.log("inside handleFirstConnectivityChange" );
    NetInfo.isConnected.fetch().then(isConnected => {
      if (isConnected) {
        //this.dogenerateToken(this.state.nameValue, this.state.pwdValue);
        this.setState({
          isOnline: true
        })
      } else {
        this.setState({
          isOnline: false
        })
      }
    });


    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
    // this.setState({
    //   isOnline: isConnected
    // })
  };

    render() {
      return (
        <View style={styles.container}>
           
            <TextInput
                    style={styles.InputTextContent}
                    placeholder={R.strings.login.lbl_name}
                    returnKeyType="next"
                    onChangeText={(text) => this.setState({nameValue: text})}
                />

            <Text style={styles.ErrorText}>{this.state.nameError}</Text>
            
            <TextInput
                    style={styles.InputTextContent}
                    placeholder={R.strings.login.lbl_email}
                    returnKeyType="next"
                    keyboardType='email-address'
                    onChangeText={(text) => this.setState({emailValue: text})}
                />

            <Text style={styles.ErrorText}>{this.state.emailError}</Text>
            
            <TextInput
                    style={styles.InputTextContent}
                    placeholder={R.strings.login.lbl_email}
                    returnKeyType="next"
                    keyboardType='number-pad'
                    onChangeText={(text) => this.setState({phoneValue: text})}
                />

            <Text style={styles.ErrorText}>{this.state.phoneError}</Text>
          
            <RaisedButton
                onPress={() => {
                  this.state.nameError = "";
                  this.state.emailError = "";
                  this.state.phoneError = "";
                  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                  if (this.state.nameValue.trim() === "") {
                    this.setState(() => ({ nameError: R.strings.login.msg_required_name }));
                  } 
                  else if(this.state.emailValue.trim() ===""){
                    this.setState(() => ({ emailError: R.strings.login.msg_required_email }));
                  }
                  else if (reg.test(this.state.emailValue.trim()) == false) {
                    this.setState(() => ({ emailError: R.strings.login.msg_incorrect_email }));
                  }
                  else if(this.state.phoneValue.trim() ===""){
                    this.setState(() => ({ phoneError: R.strings.login.msg_required_phone }));
                  }
                  else if (this.state.phoneValue.length != 10) {
                    this.setState(() => ({ phoneError: R.strings.login.msg_incorrect_phone}));
                  }  else {
                    //this.props.navigation.navigate('WheelScreen');
                    console.log("inside else");
                    if (this.state.isOnline) {
                      //this.doCallPriceAPI(this.state.nameValue, this.state.emailValue,this.state.phoneValue);
                        this.props.navigation.navigate('WheelScreen',{name: this.state.nameValue,email: this.state.emailValue,phone: this.state.phoneValue})
                    } else {
                      if (this.state.alertShowing) {
                        this.state.alertShowing = false
                        Alert.alert(
                          R.strings.common.app_title,
                          R.strings.common.msg_internet_connection,
                          [
                            {
                              text: R.strings.login.lbl_ok,
                              onPress: () => { this.state.alertShowing = true }
                            }
                          ],
                          { cancelable: false }
                        );
                      }
                    }

                  }
                }}
                style={styles.LoginButtonContent}
                rippleColor={'red'}
                shadeColor={'red'}
              >
                <Text
                  style={{ color: "white", fontSize: 15, fontWeight: "bold" }}
                >
                  Login
                </Text>
              </RaisedButton>

        </View>
      );
    }

  }


  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    InputTextContent: {
        borderRadius: 12,
        padding: 4,
        height: 45,
        marginLeft:20,
        backgroundColor : "#E5E5E5",
        marginRight:20,
        marginTop: 5,
        borderColor: "gray",
        borderWidth: 1,
        width: "100%",
  },
  ErrorText: {
    marginTop : 0,
    marginLeft:20,
    margin: 1,
    color: "red"
    },
    LoginButtonContent: {
		height: 45,
		marginLeft:20,
		marginRight:20,
        marginTop: 12,
        paddingTop: 12,
        paddingBottom: 12,
        paddingLeft: 18,
        fontWeight: "bold",
        paddingRight: 18,
        backgroundColor: "red",
        borderRadius: 12,
        borderColor: "#E5E5E5",
        alignItems: "center",
        borderWidth: 1,
  },
  });