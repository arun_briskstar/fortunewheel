/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStackNavigator, createAppContainer} from 'react-navigation';
import SplashScreen from "./app/screens/SplashScreen";
import WheelScreen from "./app/screens/WheelScreen";
import LoginPage from "./app/screens/LoginPage";

const MainNavigator = createStackNavigator({
  Splash: {screen: SplashScreen},
  WheelScreen:{screen: WheelScreen},
  LoginPage: {screen: LoginPage}
});

const App = createAppContainer(MainNavigator);

export default App;
