const strings = {
    common: {
        msg_internet_connection: 'Please Check Your Internet Connection',
    },
    login: {
        msg_exit_app: 'Are you sure you want to exit?',
        lbl_cancel: "Cancel",
        lbl_yes: 'Yes',
        lbl_phone_number: 'Phone Number',
        lbl_name: 'Name',
        lbl_email: 'Email-Id',
        msg_required_email: 'Email is required.',
        msg_incorrect_email: 'Enter valid email.',
        msg_required_phone: 'Phone number is required.',
        msg_incorrect_phone: 'Enter valid Phone number.',
        msg_required_name: 'Name is required.',
        lbl_ok: 'Ok',
        lbl_error: 'Error',
        lbl_exit: 'Exit?'
    },
   
}
export default strings