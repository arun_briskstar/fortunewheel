/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

type Props = {};
export default class SplashScreen extends Component<Props> {

  constructor(props) {
    super(props);
  }

  componentDidMount(){

    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(()=>{
      this.props.navigation.navigate('LoginPage')
    }, 5000);
}

componentWillUnmount(){
    clearTimeout(this.timeoutHandle); 
}

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Fortune Wheel</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
